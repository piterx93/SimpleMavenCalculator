package pl.ncdc.hot;

import org.junit.Assert;
import org.junit.Test;

public class CalcTest {

	@Test
	public void shouldSumResult() {
		//
		int FirstValue = 15;
		int SecondValue = 5;
		double Summary = FirstValue + SecondValue;
		
		//
		double CheckSum = Calc.sum(FirstValue, SecondValue);
		//
		Assert.assertTrue(Summary == CheckSum);
	}
	@Test
	public void shouldDiffResult() {
		//
		int FirstValue = 15;
		int SecondValue = 5;
		double Summary = FirstValue - SecondValue;
		
		//
		double CheckDiff = Calc.diff(FirstValue, SecondValue);
		//
		Assert.assertTrue(Summary == CheckDiff);
	}
	@Test
	public void shouldMainRunsCorrectly() {
		//
		if(Calc.main(null) == 0)
			Assert.assertTrue(true);
		else
			Assert.assertTrue(false);
	}
	@Test
	public void shouldMultiResult() {
		//
		int FirstValue = 15;
		int SecondValue = 5;
		double Summary = FirstValue * SecondValue;
		
		//
		double CheckMulti = Calc.multi(FirstValue, SecondValue);
		//
		Assert.assertTrue(Summary == CheckMulti);
	}

}
